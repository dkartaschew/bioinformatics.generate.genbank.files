Generate Dummy GenBank files
==================================
----
Test bed application for generation of GenBank files for testing purposes.

Install
-------------

* run **$ mvn clean package** to build.
* The jar file will be available in the /target folder.

