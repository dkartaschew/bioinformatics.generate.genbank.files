/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.generategenbankfiles;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main Application class and entry point
 *
 * @author Darran Kartaschew
 */
public class App {

    /**
     * The minimum number of files.
     */
    private static final int MIN_FILES = 1;
    /**
     * The minimum size of the sequence to generate.
     */
    private static final int MIN_SEQUENCE_SIZE = 20;
    /**
     * The maximum size of the sequence to generate.
     */
    private static final int MAX_SEQUENCE_SIZE = 32000000;
    /**
     * Number of base pair per line.
     */
    private static final int BP_PER_LINE = 60;
    /**
     * Number of BP per group on a line
     */
    private static final int BP_PER_GROUP = 10;
    /**
     * Number of groups per line
     */
    private static final int GROUP_PER_LINE = 6;
    /**
     * The default organism name.
     */
    private static final String ORGANISM_NAME = "Artificial Life Form %d";
    /**
     * Filename format to use when creating the file.
     */
    private static final String FILENAME_FORMAT = "ALF%06d";
    private static final String FILENAME_EXTENSION = ".gbk";
    /**
     * Format information.
     */
    private static final String LOCUS = "LOCUS       %s            %7d bp    DNA     circular BCT 05-MAR-2010\n";
    private static final String SOURCE = "SOURCE      %s \n  ORGANISM  %s \n";
    private static final String ORIGIN = "ORIGIN\n";
    private static final String SEQUENCE = " %8d%s\n";
    private static final String ORIGIN_END = "//";
    /**
     * Sequence characters.
     */
    private static final char[] bases = new char[]{'a', 'g', 'c', 't'};
    /**
     * Application header.
     */
    private final static String ApplicationHeader =
            "Generate GenBank Files v1.0.0\n"
            + "-----------------------------\n\n"
            + "Copyright 2013, Darran Kartaschew,\n"
            + "Apache License, Version 2.0\n\n"
            + "GenBank File generater for testing ES and related activities.\n";
    /**
     * Application Help.
     */
    private final static String ApplicationHelp =
            "Usage: java -jar GenerateGenBankFiles <outputPath> <numberOfFiles> <minSequenceSize> <maxSequenceSize>\n"
            + "       <outputPath>      - The location for the files to be placed once generated.\n"
            + "       <numberOfFiles>   - The number of files to generate.\n"
            + "       <minSequenceSize> - The smallest sequence to generate. (Must be above " 
            + Integer.toString(MIN_SEQUENCE_SIZE) + ").\n"
            + "       <maxSequenceSize> - The largest sequence to generate. (Must be below " 
            + Integer.toString(MAX_SEQUENCE_SIZE) + ").\n";
    /**
     * The file path to store the generated files.
     */
    private String filePath;
    /**
     * The number of files to generate.
     */
    private int numberOfFiles;
    /**
     * The minimum sequence size.
     */
    private int minSequenceSize;
    /**
     * The maximum sequence size.
     */
    private int maxSequenceSize;

    /**
     * Main entry point.
     *
     * @param args CLI arguments.
     */
    public static void main(String[] args) {
        new App().run(args);
    }

    /**
     * Main entry point.
     *
     * @param args CLI arguments.
     */
    public void run(String[] args) {
        printHeader();
        try {
            parseCLI(args);
        } catch (Exception ex) {
            printHelp(ex.getMessage());
            System.exit(0);
        }
        printParameters();
        try {
            generateFiles();
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Print Help to console for application usage.
     */
    private void printHelp(String message) {
        System.out.println("Warning: " + message);
        System.out.println(ApplicationHelp);
    }

    /**
     * Print application header information.
     */
    private void printHeader() {
        System.out.println(ApplicationHeader);
    }

    /**
     * Attempts to parse the command line for runtime parameters.
     *
     * @param args command line arguments
     * @throws Exception If the parsing fails in any manner.
     */
    private void parseCLI(String[] args) throws Exception {
        if (args.length < 4) {
            throw new Exception("Missing command line arguments");
        }
        filePath = args[0];
        // Check the path exists and is a directory.
        File path = new File(filePath);
        if (!path.exists() || !path.isDirectory()) {
            throw new Exception("Path specified appears to not exist or is invalid.");
        }
        filePath = path.getAbsolutePath();
        // Check the number of files.
        numberOfFiles = Integer.parseInt(args[1]);
        if (numberOfFiles < MIN_FILES) {
            throw new Exception("Number of files to generate is less than 1.");
        }
        // min and max sequence size.
        minSequenceSize = Integer.parseInt(args[2]);
        maxSequenceSize = Integer.parseInt(args[3]);
        if (minSequenceSize < MIN_SEQUENCE_SIZE || maxSequenceSize > MAX_SEQUENCE_SIZE || minSequenceSize > maxSequenceSize) {
            throw new Exception("The sequence size selected is out of required bounds.");
        }
        if (minSequenceSize == maxSequenceSize) {
            maxSequenceSize++;
        }
    }

    /**
     * Print all parameters as parsed to the command line.
     */
    private void printParameters() {
        System.out.println("Parameters:");
        System.out.println(" Path:      " + filePath);
        System.out.println(String.format(" Num Files: %d", numberOfFiles));
        System.out.println(String.format(" Min Size:  %d", minSequenceSize));
        System.out.println(String.format(" Max Size:  %d\n", maxSequenceSize));
    }

    /**
     * Generate File based on the parameters passed.
     */
    private void generateFiles() throws IOException {
        Random randomGenerator = new Random();
        for (int organismNumber = 1; organismNumber <= numberOfFiles; organismNumber++) {
            // Generate the filename
            String filename = filePath + java.io.File.separatorChar + String.format(FILENAME_FORMAT, organismNumber) 
                    + FILENAME_EXTENSION;
            String organism = String.format(ORGANISM_NAME, organismNumber);
            String organismID = String.format(FILENAME_FORMAT, organismNumber);
            int basePairs = randomGenerator.nextInt(maxSequenceSize - minSequenceSize) + minSequenceSize;
            generateFile(filename, organismID, organism, basePairs);
            System.out.print(".");
        }
        System.out.println();
    }

    /**
     * Generate a single GenBank file with the supplied parameters.
     *
     * @param filename The filename to use.
     * @param organismID The GenBank ID for this organism.
     * @param organism The organism name.
     * @param bp The number of base pairs to generate.
     */
    private void generateFile(String filename, String organismID, String organism, int bp) throws IOException {
        Random rnd = new Random();
        // Open the file for writing.
        File file = new File(filename);
        file.createNewFile();
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file), bp + 200)) {
            // Write out header information.
            out.write(String.format(LOCUS, organismID, bp));
            out.write(String.format(SOURCE, organism, organism));
            out.write(ORIGIN);
            // Generate our DNA sequence.
            int line = 1;
            while (bp > BP_PER_LINE) {
                StringBuilder seq = new StringBuilder();
                // We have 6 x 10 elements to write.
                for (int i = 0; i < GROUP_PER_LINE; i++) {
                    seq.append(' ');
                    for (int j = 0; j < BP_PER_GROUP; j++) {
                        seq.append(bases[rnd.nextInt(bases.length)]);
                    }
                }
                out.write(String.format(SEQUENCE, line, seq.toString()));
                bp -= BP_PER_LINE;
                line += BP_PER_LINE;
            }
            // Less than 60 to go.
            if (bp > 0) {
                StringBuilder seq = new StringBuilder();
                for (int j = 0; j < bp; j++) {
                    // Add in our space if needed.
                    if (j % BP_PER_GROUP == 0 && (j - 1) != bp) {
                        seq.append(' ');
                    }
                    seq.append(bases[rnd.nextInt(bases.length)]);

                }
                out.write(String.format(SEQUENCE, line, seq.toString()));
            }
            // Finalise file.
            out.write(ORIGIN_END);
        }
    }
}
